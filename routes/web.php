<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [
	'as' => 'trang-chu',
	'uses' => 'PageController@getIndex'
]);

Route::get('loaisanpham/{type?}', [
	'as' => 'loaisanpham',
	'uses' => 'PageController@getLoaiSp'
]);

Route::get('chitietsanpham/{id?}', [
	'as' => 'chitietsanpham',
	'uses' => 'PageController@getChitiet'
]);

Route::get('lienhe', [
	'as' => 'lienhe',
	'uses' => 'PageController@getLienhe'
]);

Route::get('gioithieu', [
	'as' => 'gioithieu',
	'uses' => 'PageController@getGioithieu'
]);

Route::get('add-to-cart\{id?}', [
	'as' => 'themgiohang',
	'uses' => 'PageController@getAddtocart'
]);

Route::get('del-cart\{id?}', [
	'as' => 'xoagiohang',
	'uses' => 'PageController@getDelitemcart'
]);

Route::get('dat-hang', [
	'as' => 'dathang',
	'uses' => 'PageController@postCheckout1'
]);

Route::post('dat-hang', [
	'as' => 'dathang',
	'uses' => 'PageController@postCheckout'
]);

Route::get('dang-ky', [
	'as' => 'dangky',
	'uses' => 'PageController@getDangKy'
]);

Route::post('dang-ky', [
	'as' => 'dangky',
	'uses' => 'PageController@postDangKy'
]);

Route::get('dang-nhap', [
	'as' => 'dangnhap',
	'uses' => 'PageController@getDangNhap'
]);

Route::post('dang-nhap', [
	'as' => 'dangnhap',
	'uses' => 'PageController@postDangNhap'
]);

Route::get('dang-xuat', [
	'as' => 'dangxuat',
	'uses' => 'PageController@getDangXuat'
]);

Route::get('admin/dangxuat', [
	'as' => 'dangxuat',
	'uses' => 'PageController@getDangXuatAdmin'
]);

//quan tri
Route::get('admin/login', [
	'as' => 'login',
	'uses' => 'PageController@getLoginAdmin'
]);

Route::post('admin/login', [
	'as' => 'login',
	'uses' => 'PageController@postLoginAdmin'
]);

Route::get('admin/index', [
	'as' => 'indexAdmin',
	'uses' => 'PageController@getIndexAdmin'
]);

Route::get('admin/sanpham', [
	'as' => 'sanpham',
	'uses' => 'PageController@getSanPham'
]);

Route::post('admin/capnhat', [
	'as' => 'capnhat',
	'uses' => 'PageController@postCapNhat'
]);

Route::post('admin/themsanpham', [
	'as' => 'themsanpham',
	'uses' => 'PageController@postThemSanPham'
]);

Route::post('admin/xoasanpham', [
	'as' => 'xoasanpham',
	'uses' => 'PageController@postXoaSanPham'
]);

Route::get('admin/slide', [
	'as' => 'slide',
	'uses' => 'PageController@getSlide'
]);

Route::post('admin/capnhatslide', [
	'as' => 'capnhatslide',
	'uses' => 'PageController@postCapNhatSlide'
]);

Route::post('admin/themslide', [
	'as' => 'themslide',
	'uses' => 'PageController@postThemSlide'
]);

Route::post('admin/xoaslide', [
	'as' => 'xoaslide',
	'uses' => 'PageController@postXoaSlide'
]);

Route::get('admin/donhang', [
	'as' => 'donhang',
	'uses' => 'PageController@getDonHang'
]);

Route::post('admin/tiepnhandonhang', [
	'as' => 'tiepnhandonhang',
	'uses' => 'PageController@postTiepNhanDonHang'
]);

Route::post('admin/xulydonhang', [
	'as' => 'xulydonhang',
	'uses' => 'PageController@postXuLyDonHang'
]);

Route::get('admin/khachhang', [
	'as' => 'khachhang',
	'uses' => 'PageController@getKhachHang'
]);

Route::get('admin/thanhvien', [
	'as' => 'thanhvien',
	'uses' => 'PageController@getThanhVien'
]);


