<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'id_type' => function() {
        	return factory(App\ProductType::class)->create()->id;
        },
        'description' => $faker->text(),
        'unit_price' => $faker->randomDigit,
        'promotion_price' => $faker->randomDigit ,
        'image' => $faker->imageUrl(),
        'unit' => $faker->imageUrl(),
        'new' => $faker->randomDigit
    ];
});
