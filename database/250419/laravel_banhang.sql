-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 25, 2019 lúc 03:35 PM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `laravel_banhang`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bills`
--

CREATE TABLE `bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `date_order` date DEFAULT NULL,
  `total` float DEFAULT NULL COMMENT 'tổng tiền',
  `payment` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'hình thức thanh toán',
  `note` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirm` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `bills`
--

INSERT INTO `bills` (`id`, `id_customer`, `date_order`, `total`, `payment`, `note`, `confirm`, `created_at`, `updated_at`) VALUES
(24, 24, '2019-04-25', 21559000, 'COD', 'gfghk', 2, '2019-04-25 10:19:21', '2019-04-25 10:19:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill_detail`
--

CREATE TABLE `bill_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_bill` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) NOT NULL,
  `quantity` int(11) NOT NULL COMMENT 'số lượng',
  `unit_price` double NOT NULL,
  `promotion_price` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `bill_detail`
--

INSERT INTO `bill_detail` (`id`, `id_bill`, `id_product`, `quantity`, `unit_price`, `promotion_price`, `created_at`, `updated_at`) VALUES
(34, 24, 67, 1, 3899000, 0, '2019-04-25 09:46:21', '2019-04-25 09:46:21'),
(33, 24, 66, 1, 18990000, 17660000, '2019-04-25 09:46:21', '2019-04-25 09:46:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `name`, `gender`, `email`, `address`, `phone_number`, `note`, `created_at`, `updated_at`) VALUES
(17, 'Nguyễn Thu Thủy', 'nam', 'thuynguyen@gmail.com', 'Hà Nội', '0982337337', 'Giao sớm cho mình nha', '2019-04-25 07:37:23', '2019-04-25 07:37:23'),
(18, 'Lê Hữu Minh Tiến', 'nam', 'tienlee.ask@gmail.com', 'Lê Thanh Nghị', '0964979247', 'Giao nhanh giúp mình nhé :)', '2019-04-25 08:47:53', '2019-04-25 08:47:53'),
(19, 'Nguyễn Thu Thủy', 'nữ', 'thuynguyen@gmail.com', 'Hà Nội', '0982334788', 'Giao sớm nha shop', '2019-04-25 08:56:30', '2019-04-25 08:56:30'),
(20, 'Nguyễn Thu Thủy', 'nữ', 'thuynguyen@gmail.com', 'Hà Nội', '0982334788', 'Giao nhanh giúp mình nha', '2019-04-25 09:10:25', '2019-04-25 09:10:25'),
(21, 'Nguyễn Hồng Vân', 'nữ', 'nguyenhongvan@gmail.com', 'Hà Nội', '0964977288', 'giao sớm mình nha', '2019-04-25 09:38:22', '2019-04-25 09:38:22'),
(22, 'Lê Hữu Minh Tiến', 'nam', 'tienlee.ask@gmail.com', 'Lê Thanh Nghị', '0964979247', 'aaa', '2019-04-25 09:43:26', '2019-04-25 09:43:26'),
(23, 'Lê Hữu Minh Tiến', 'nam', 'tienlee.ask@gmail.com', 'Lê Thanh Nghị', '0964979247', 'aasas', '2019-04-25 09:44:38', '2019-04-25 09:44:38'),
(24, 'Lê Hữu Minh Tiến', 'nam', 'tienlee.ask@gmail.com', 'Lê Thanh Nghị', '0964979247', 'gfghk', '2019-04-25 09:46:21', '2019-04-25 09:46:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_type` int(10) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `unit_price` float DEFAULT NULL,
  `promotion_price` float DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `id_type`, `description`, `unit_price`, `promotion_price`, `image`, `unit`, `new`, `created_at`, `updated_at`) VALUES
(64, 'Samsung Galaxy S10+ (128GB', 8, 'Chiếc điện thoại màn hình Infinity-O lớn nhất, camera xuất sắc nhất và hiệu năng mạnh mẽ nhất của Samsung đã xuất hiện. Galaxy S10+ dẫn đầu xu thế, mang trên mình các công nghệ tiên tiến của tương lai và là một tác phẩm nghệ thuật đích thực.\r\nKiệt tác màn hình vô cực Infinity-O\r\nGần như toàn bộ phần viền màn hình đã được loại bỏ trên Samsung Galaxy S10+, không có tai thỏ, không bị hạn chế tầm nhìn, trước mắt bạn là màn hình cực lớn 6,4 inch hiển thị vô cùng sống động. Viền cong siêu mỏng tràn cả 4 cạnh kết hợp cùng các đường cắt laser chuẩn xác khéo léo giấu kín camera nằm ngay trên màn hình. Samsung S10+ xứng đáng được gọi là một kiệt tác ngay trên tay bạn.', 22990000, 20990000, '636863648672228468_ss-galaxy-s10-plus-trang-1.png', 'Sản phẩm', 1, '2019-04-25 04:08:01', '2019-04-25 04:10:43'),
(65, 'iPhone X 64GB', 8, 'iPhone X 64GB đã lột xác hoàn toàn với việc loại bỏ nút Home truyền thống, màn hình tràn viền và camera kép ở phía sau đã được đặt lại vị trí theo chiều dọc. Khung viền từ thép sáng bóng bền bỉ và mặt lưng kính với các góc bo tròn dễ dàng cầm nắm. Có thể nói đây là một thiết kế khá đột phá mà lâu lắm rồi Apple mới thể hiện lại. Người dùng cần phải trên tay thì mới cảm nhận được hết nét tinh tế và sang trọng của sản phẩm.\r\nMàn hình của iPhone X 64GB hiển thị đẹp hơn\r\niPhone X 64GB là chiếc smartphone đầu tiên được Apple ưu ái cho tấm nền màn hình OLED, kích thước 5.8 inch và độ phân giải đạt chuẩn Super Retina HD, Điều này giúp cho màn hình có màu sắc sống động, góc nhìn rộng hơn, cải thiện độ sáng và tốn ít điện năng hơn. Bên cạnh đó, công nghệ True Tone còn giúp màu sắc trở nên cực kì trung thực.', 25990000, 23990000, '636483223586180190_3.jpg', 'Sản phẩm', NULL, '2019-04-25 04:09:26', '2019-04-25 04:09:26'),
(66, 'Huawei P30 Lite', 10, 'Với P30 Lite, bạn sẽ tìm thấy những trải nghiệm trọn vẹn nhất về hiệu năng, khả năng chụp ảnh, selfie cũng như cảm quan thẩm mĩ. Tất cả đều được gói gọn vào trong một thiết bị di động gọn gàng có giá bán tầm trung hợp túi tiền.\r\n\r\nDiện mạo sang trọng, thanh lịch và bóng bẩy\r\nNhà sản xuất điện thoại lớn thứ 3 thế giới đã thể hiện sự tinh tế trong cách thiết kế sản phẩm khi trau chuốt tỉ mỉ từng đường nét trên diện mạo P30 Lite. Việc vuốt cong hai cạnh mặt lưng thiết bị đem tới cảm giác cầm nắm hết sức gọn gàng, cho phép người dùng dễ dàng thao tác trên chiếc điện thoại chỉ bằng một tay. Ngôn ngữ thiết kế kim loại – kính bóng bẩy với những tùy chọn màu sắc thanh lịch đem tới cho P30 Lite cảm quan thẩm mĩ hết sức tuyệt vời.', 18990000, 17660000, '636892756397245509_huawei-p30-lite-den-1.png', 'Sản phẩm', 1, '2019-04-25 04:12:28', '2019-04-25 04:12:28'),
(67, 'Xiaomi Redmi 5 Plus', 11, 'Redmi 5 Plus sở hữu cho mình vẻ bề ngoài sang trọng và tinh tế không kém gì các thiết bị cao cấp. Máy sở hữu thiết kế nguyên khối với khung kim loại chắc chắn. Các góc cạnh được bo cong mềm mại cho cảm giác cầm nắm thoải mái. Máy cũng được trang bị mặt kính cong 2.5D ở mặt trước cho cảm giác vuốt từ cạnh màn hình \"đã\" hơn.', 3899000, 0, '636549777491044706_1.jpg', 'Sản phẩm', 1, '2019-04-25 04:13:40', '2019-04-25 04:13:40'),
(68, 'OPPO F11', 12, 'Với chiếc điện thoại F11, OPPO cho thấy sự thấu hiểu khách hàng đến mức tối đa khi họ mang tới cho sản phẩm này những công nghệ và tính năng vượt trội hoàn toàn so với mức giá bán ra. Sở hữu OPPO F11, bạn sẽ có được trải nghiệm cao cấp bậc nhất được gói gọn trong một smartphone có giá bán tầm trung hấp dẫn.\r\n\r\nThiết kế nịnh mắt, đẹp huyền bí và sang trọng\r\nOPPO F11 được trau chuốt từng đường nét trên thân máy với độ hoàn thiện tối ưu nhằm mang tới cho người dùng trải nghiệm cầm nắm tuyệt hảo. Nhà sản xuất đã khoác lên mình F11 những sắc màu toát nên vẻ sang trọng như tím thạch anh và xanh ngọc thạch, cho phép thay đổi tông màu khi nghiêng máy dưới những góc nhìn khác nhau tạo nên vẻ đẹp huyền bí nhưng không kém phần sang trọng. Mặt lưng sản phẩm được vuốt cong nhẹ nhàng về hai phía một cách tinh tế, từng đường nét nhỏ trên OPPO F11 như cảm biến vân tay, camera sau và các cụm phím bấm đều được đặt ở vị trí hết sức hợp lý, thuận tiện cho việc thao tác.', 7290000, 6990000, '636888737679482202_oppo-f11-xanh-1.png', 'Sản phẩm', 1, '2019-04-25 04:16:34', '2019-04-25 04:16:34'),
(69, 'Samsung Galaxy S8 Plus', 9, 'Samsung S8 Plus ra mắt đã làm hài lòng giới công nghệ lẫn người tiêu dùng hâm mộ hãng điện thoại danh tiếng đến từ Hàn Quốc – Samsung. Thiết kế có thể chưa được xem là đột phá hoàn toàn nhưng ngay từ cái nhìn đầu tiên đã mang lại cảm xúc thán phục bởi sự sắc sảo, tinh tế đến từ chi tiết và một màn hình cong tràn cạnh “không viền” đáng ngạc nhiên. Kèm theo là nhiều tính năng đặc biệt thú vị nhằm nâng cao trải nghiệm cho người dùng. S8 Plus hứa hẹn sẽ mang lại sự thành công mới cho Samsung trong năm nay.\r\nTrong khi thiết kế các dòng smartphone hiện nay đa số đi theo lối mòn quen thuộc thì Galaxy S8 Plus vẫn tự tin khoác lên mình một vẻ đẹp khó cưỡng lại. Đó là sự tổng hợp của kính, kim loại và một màn hình lớn với viền “vô cực”.', 14490000, 13990000, '636396217066191623_1.jpg', 'Sản phẩm', 1, '2019-04-25 04:18:04', '2019-04-25 04:18:04');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `link` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `link`, `image`) VALUES
(10, 'source/image/product/slide636916544586329304_Banner-C1_honor 20 lite.png', '636916544586329304_Banner-C1_honor 20 lite.png'),
(11, 'source/image/product/slide636896767527083579_C1-31.png', '636896767527083579_C1-31.png'),
(12, 'source/image/product/slide636906876763309026_Banner-NokiaThang4-C1-2x.png', '636906876763309026_Banner-NokiaThang4-C1-2x.png'),
(13, 'source/image/product/slide636903388068045499_Banner-Huawei P30 Lite-C1_1200x300@2x.png', '636903388068045499_Banner-Huawei P30 Lite-C1_1200x300@2x.png'),
(14, 'source/image/product/slide636911178524982692_Banner-C1_redmi 7.jpg', '636911178524982692_Banner-C1_redmi 7.jpg'),
(18, 'source/image/product/slide636896751900580555_Samsung-Thang4-C1.jpg', '636896751900580555_Samsung-Thang4-C1.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `type_products`
--

CREATE TABLE `type_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `type_products`
--

INSERT INTO `type_products` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
(8, 'Iphone', 'no', 'no', NULL, NULL),
(9, 'Samsung', 'no description', 'no', NULL, NULL),
(10, 'Huawei', 'no', 'no', NULL, NULL),
(11, 'Xiaomi', 'no', 'no', NULL, NULL),
(12, 'Oppo', 'no', 'no', NULL, NULL),
(13, 'Asus', 'no', 'no', NULL, NULL),
(14, 'Vsmart', 'no', 'no', NULL, NULL),
(15, 'Sony', 'no', 'no', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `phone`, `address`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(10, 'Le Huu Minh Tien', 'tienlee.ask@gmail.com', '$2y$10$CM6mgTbq/E16IEkowFvntuWnSmIHFzIGj8TbA1NFpWGgJFBv3ctwG', '0964979247', 'HN', 1, NULL, '2019-04-25 12:49:50', '2019-04-25 12:49:50'),
(11, 'Nguyen Thi Hoa', 'nguyenhoa221@gmail.com', '$2y$10$6Uj6kx9bwW9cqY8yNyWp8.rg18x4K40ZeNAlUsza0rEQqDu.a9W1O', '0964979223', 'Ha Noi', 0, NULL, '2019-04-25 12:54:02', '2019-04-25 12:54:02');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bills_ibfk_1` (`id_customer`);

--
-- Chỉ mục cho bảng `bill_detail`
--
ALTER TABLE `bill_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_detail_ibfk_2` (`id_product`),
  ADD KEY `id_bill` (`id_bill`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_id_type_foreign` (`id_type`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `type_products`
--
ALTER TABLE `type_products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bills`
--
ALTER TABLE `bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `bill_detail`
--
ALTER TABLE `bill_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `type_products`
--
ALTER TABLE `type_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_id_type_foreign` FOREIGN KEY (`id_type`) REFERENCES `type_products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
