@extends('master')
@section('content')
	<div class="container">
		<div id="content">
			
		<form action="{{ route('dangky') }}" method="post" class="beta-form-checkout">
			@csrf
				<div class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						@if(count($errors) > 0)
						<div class="alert alert-danger">
							@foreach($errors->all() as $err)
								{{ $err }}
							@endforeach
						</div>
						@endif

						@if(Session::has('thongbao'))
							<div class="alert alert-success">
								{{ Session::get('thongbao') }}
							</div>
						@endif
						<h4>Đăng kí tài khoản</h4>
						<div class="space20">&nbsp;</div>

						
						<div class="form-block">
							<label for="email">Địa chỉ email*</label>
							<input type="email" name="email" id="email" required>
						</div>

						<div class="form-block">
							<label for="your_last_name">Họ và tên*</label>
							<input type="text" id="hoten" name="hoten" required>
						</div>

						<div class="form-block">
							<label for="adress">Địa chỉ nơi ở*</label>
							<input type="text" id="diachi" name="diachi" value="" required>
						</div>


						<div class="form-block">
							<label for="phone">Số điện thoại*</label>
							<input type="text" id="sodienthoai" name="sodienthoai" required>
						</div>
						<div class="form-block">
							<label for="phone">Mật khẩu*</label>
							<input type="password" id="password" name="password" style="height: 37px; border: 1px solid #e1e1e1;" required>
						</div>
						<div class="form-block">
							<label for="phone">Nhập lại mật khẩu*</label>
							<input type="password" id="repassword" name="repassword" style="height: 37px; border: 1px solid #e1e1e1;" required>
						</div>
						<div class="form-block">
							<button type="submit" name="dangky" class="btn btn-primary">Đăng ký tài khoản</button>
						</div>
					</div>
					<div class="col-sm-3"></div>
				</div>
			</form>
		</div> <!-- #content -->
	</div> <!-- .container -->
	@endsection