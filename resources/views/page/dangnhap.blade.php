@extends('master')
@section('content')	
	<div class="container">
		<div id="content">
			
		<form action="{{ route('dangnhap') }}" method="post" class="beta-form-checkout">
				@csrf
				<div class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						@if(Session::has('flag'))
							<div class="alert alert-{{ Session::get('flag') }}">{{ Session::get('message') }}</div>
						@endif
						<h4>Đăng nhập tài khoản</h4>
						<div class="space20">&nbsp;</div>

						
						<div class="form-block">
							<label for="email">Địa chỉ email*</label>
							<input type="email" id="email" name="email" required>
						</div>
						<div class="form-block">
							<label for="email">Mật khẩu*</label>
							<input type="password" id="password" name="password" style="height: 37px; border: 1px solid #e1e1e1;" required>
						</div>
						<div class="form-block">
							<button type="submit" name="dangnhap" class="btn btn-primary">Đăng nhập</button>
						</div>
					</div>
					<div class="col-sm-3"></div>
				</div>
			</form>
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection