@extends('admin/master')
@section('contentcp')
    <!-- Begin page content -->
     <!-- Begin page content -->
     <div class="container">
            <div class="col-md-6 col-md-push-3" style="margin-top: 100px;">
                    @if(Session::has('message'))
                        <div class="alert alert-{{ Session::get('flag') }}">{{ Session::get('message') }}</div>
                    @endif
        
                  @if(count($errors) > 0)
                    @foreach($errors->all() as $er)
                        <div class="alert alert-danger">{{ $er }}</div>
                    @endforeach
                  @endif
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="form-group">
                  <label for="exampleInputEmail1">Địa chỉ email</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Mật khẩu</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password" required>
                </div>
                <button type="submit" name="submit" class="btn btn-info">Đăng nhập quản trị</button>
              </form>
            </div>
          </div>
      

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#khachhang').DataTable({
          "pagingType": "full_numbers",
          "language": {
              "search": "Tìm kiếm:",
          }
        });
      });
    </script>
@endsection