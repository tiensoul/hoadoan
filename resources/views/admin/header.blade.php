<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ route('indexAdmin')}}">TRANG QUẢN TRỊ</a>
        </div>
        @if(Auth::check())
        <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
        <li><a href="{{ route('indexAdmin')}}">Trang chủ</a></li>
            <li><a href="{{ route('sanpham')}}">Sản phẩm</a></li>
            <li><a href="{{ route('slide')}}">Trình chiếu</a></li>
            <li><a href="{{ route('donhang') }}">Đơn hàng</a></li>
            <li><a href="{{ route('khachhang') }}">Khách hàng</a></li>
            <li><a href="{{ route('thanhvien') }}">Thành viên</a></li>
            <li><a href="admin/index">Giới thiệu</a></li>
            @if(Auth::check())
                <li><a href="{{ route('dangxuat') }}">Đăng xuất</a></li>
                <li><a href="admin/index">Xin chào, {{ Auth::user()->full_name }}</a></li>
            @else
                <li><a href="{{ route('login') }}">Đăng nhập</a></li>
            @endif
        </ul>
        </div><!--/.nav-collapse -->
        @endif
    </div>
</nav>