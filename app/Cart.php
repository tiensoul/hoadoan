<?php

namespace App;

class Cart
{
	public $items = null;
	public $totalQty = 0;
	public $totalPrice = 0;
	public $promotionPrice = 0;

	public function __construct($oldCart){
		if($oldCart){
			$this->items = $oldCart->items;
			$this->totalQty = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
			$this->promotionPrice = $oldCart->promotionPrice;
		}
	}

	public function add($item, $id){
		$giohang = ['qty'=>0, 'price' => $item->unit_price, 'promotion_price' => $item->promotion_price, 'item' => $item];
		if($this->items){
			if(array_key_exists($id, $this->items)){
				$giohang = $this->items[$id];
			}
		}
		$giohang['qty']++;
		if($giohang['promotion_price'] == 0)
		{
			$giohang['price'] = $item->unit_price * $giohang['qty'];
		} else {
			$giohang['price'] = $item->promotion_price * $giohang['qty'];
		}
		$this->items[$id] = $giohang;
		$this->totalQty++;
		$this->totalPrice += $this->items[$id]['item']['promotion_price'] == 0 ? $this->items[$id]['item']['unit_price'] : $this->items[$id]['item']['promotion_price'];
	}
	//xóa 1
	public function reduceByOne($id){
		$this->items[$id]['qty']--;
		$this->items[$id]['price'] -= $this->items[$id]['item']['price'];
		$this->totalQty--;
		$this->totalPrice -= $this->items[$id]['item']['promotion_price'] == 0 ? $this->items[$id]['item']['unit_price'] : $this->items[$id]['item']['promotion_price'];
		if($this->items[$id]['qty']<=0){
			unset($this->items[$id]);
		}
	}
	//xóa nhiều
	public function removeItem($id){
		$this->totalQty -= $this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['item']['promotion_price'] == 0 ? $this->items[$id]['item']['unit_price'] : $this->items[$id]['item']['promotion_price'];
		unset($this->items[$id]);
	}
}
