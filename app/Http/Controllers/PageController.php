<?php

namespace App\Http\Controllers;
use App\Slide;
use App\Product;
use App\ProductType;
use Illuminate\Http\Request;
use App\Cart;
use Session;
use App\Customer;
use App\Bill;
use App\BillDetail;
use App\User;
use Hash;
use Auth;

class PageController extends Controller
{
    public function getIndex()
    {
        $slide = Slide::all();
        //print_r($slide);
        //exit();
    	//return view('page.trangchu', ['slide' => $slide]);

        $newproduct = Product::where('new',1)->paginate(8);
        $spkhuyenmai = Product::where('promotion_price', '<>', 0)->paginate(8);
        //print_r($newproduct);
        //exit();
        return view('page.trangchu', compact('slide', 'newproduct', 'spkhuyenmai'));
    }

    public function getLoaiSp($type)
    {
        $sptheoloai = Product::where('id_type', $type)->get();
        $spkhac = Product::where('id_type','<>', $type)->paginate(3);
        $loaisanpham = ProductType::all();
        $loaisp = ProductType::where('id', $type)->first();
        /*print_r($loaisp);
        exit();*/
    	return view('page.loaisanpham', compact('sptheoloai', 'spkhac','loaisanpham', 'loaisp'));
    }

    public function getChitiet(Request $request)
    {
        $sanpham = Product::where('id', $request->id)->first();
        $sanphamtuongtu = Product::where('id_type', $sanpham->id_type)->paginate(3);
        $spkm = Product::where('promotion_price', '<>', 0)->inRandomOrder()->limit(5)->get();
        $spm = Product::where('new', 1)->inRandomOrder()->limit(5)->get();
        // print_r($sanpham);
        // exit();
    	return view('page.chitietsanpham', compact('sanpham','sanphamtuongtu', 'spkm', 'spm'));
    }

    public function getLienhe()
    {
        return view('page.lienhe');
    }

    public function getGioithieu()
    {
        return view('page.gioithieu');
    }

    public function getAddtocart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session('cart') ? Session::get('cart') : null;
        //dd($oldCart);
        $cart = new Cart($oldCart);
        $cart->add($product, $id);
        //dd($cart);
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }
    public function getDelitemcart($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        //dd($cart);
        if(count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->back();
    }

    public function postCheckout1(Request $request)
    {
        return view('page.dathang');
    }

    public function postCheckout(Request $request)
    {
        $cart = Session::get('cart');
        //dd($cart);
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->gender = $request->gender;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phone_number = $request->phone;
        $customer->note = $request->note;
        $customer->save();

        $bill = new Bill;
        $bill->id_customer = $customer->id;
        $bill->date_order = date('Y-m-d');
        $bill->total = $cart->totalPrice;
        $bill->payment = $request->payment;
        $bill->note = $request->note;
        $bill->save();

        foreach ($cart->items as $key => $value) {
           $billdetail = new BillDetail;
            $billdetail->id_bill = $bill->id;
            $billdetail->id_product = $key;
            $billdetail->quantity = $value['qty'];
            $billdetail->unit_price = $value['item']['unit_price'];
            $billdetail->promotion_price = $value['item']['promotion_price'] == 0 ? 0 : $value['item']['promotion_price'];
            $billdetail->save();
        }
        Session::forget('cart');
        return redirect()->back()->with('thongbao', 'Đặt hàng thành công!');
    }

    public function getDangKy()
    {
        return view('page.dangky');
    }

    public function postDangKy(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60',
            'hoten' => 'required|max:50',
            'diachi' => 'required|max:255',
            'sodienthoai' => 'required|max:11',
            'password' => 'required|max:20',
            'repassword' => 'required|max:20|same:password'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống',
            'email.max' => 'Email không vượt quá 60 kí tự',
            'hoten.required' => 'Họ tên không được để trống',
            'hoten.max' => 'Họ tên không vượt quá 60 kí tự',
            'diachi.required' => 'Địa chỉ không được để trống',
            'diachi.max' => 'Địa chỉ không vượt quá 60 kí tự',
            'sodienthoai.required' => 'Số điện thoại không được để trống',
            'sodienthoai.max' => 'Số điện thoại không được vượt quá 11 kí tự',
            'password.required' => 'Mật khẩu không được để trống',
            'password.max' => 'Mật khẩu không vượt quá 20 kí tự',
            'repassword.required' => 'Trường nhập lại mật khẩu không được để trống',
            'repassword.max' => 'Trường nhập lại mật khẩu không vượt quá 20 kí tự',
            'repassword.same' => 'Mật khẩu không giống nhau'
        ]);

        $user = new User();
        $user->full_name = $request->hoten;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->sodienthoai;
        $user->address = $request->diachi;
        $user->save();

        return redirect()->back()->with('thongbao', 'Bạn đã đăng ký tài khoản thành công bây giờ bạn có thể đăng nhập!');
    }

    public function getDangNhap(Request $request)
    {
        return view('page.dangnhap');
    }

    public function postDangNhap(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60',
            'password' => 'required|max:20'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống',
            'email.max' => 'Địa chỉ email tối đa 60 kí tự',
            'password.required' => 'Mật khẩu không được để trống',
            'password.max' => 'Mật khẩu tối đa 20 kí tự'
        ]);

        $info = array('email' => $request->email, 'password' => $request->password);
        if(Auth::attempt($info)) {
            return redirect('/')->with(['flag' => 'success', 'message' => 'Đăng nhập thành công!']);
        } else {
            return redirect()->back()->with(['flag' => 'danger', 'message' => 'Đăng nhập không thành công vui lòng kiểm tra lại!']);
        }
    }

    public function getDangXuat()
    {
        Auth::logout();
        return view('page.dangnhap');
    }

    public function getDangXuatAdmin()
    {
        Auth::logout();
        return view('admin.login');
    }
    

    //admin
    public function getIndexAdmin()
    {
        return view('admin.index');
    }

    public function getSanPham()
    {
        $product_type_name = ProductType::join('products', 'type_products.id', '=', 'products.id_type')->select('products.id AS id_product', 'products.name AS name_product', 'products.description AS description_product', 
        'products.unit_price AS unit_price', 'products.promotion_price AS promotion_price', 'products.image AS image', 'products.unit AS unit', 'products.new AS new', 'type_products.name AS name_type_product')->get();
        //dd($product_type_name);

        $type_product = ProductType::all();
        return view('admin.sanpham', compact('product_type_name', 'type_product'));
    }

    public function postCapNhat(Request $request)
    {
        if($request->has('submit'))
        {
            // dd($request);
            $name = 'fileupload';
            // Thông báo khi xảy ra lỗi
            $messages = [
                
                'image' => 'Định dạng không cho phép',
                'max' => 'Kích thước file quá lớn',
            ];
            // Điều kiện cho phép upload
            $this->validate($request, [
                'fileupload' => 'image|max:10028',
            ], $messages);
            // Kiểm tra file hợp lệ
            if ($request->file($name)->isValid()){
                // Lấy tên file
                $file_name = $request->file($name)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name)->move('source/image/product',$file_name);

                // $product = new Product();
                $product = Product::find($request->idedit);
                $product->name = $request->tensp;
                $product->id_type = $request->theloai;
                $product->description = $request->mota;
                $giagoc = str_replace(".","",$request->giagoc);
                $giagoc = str_replace("đ","",$giagoc);
                $product->unit_price = $giagoc;
                $giakm = str_replace(".","",$request->giakm);
                $giakm = str_replace("đ","",$giakm);
                $product->promotion_price = $giakm;
                $product->image = $file_name;
                $product->unit = "Sản phẩm";
                $product->new = $request->spmoi;
                $product->save();

                return redirect('admin/sanpham')->with('thongbao', 'Cập nhật sản phẩm thành công!');
            }

        }
    }

    public function postThemSanPham(Request $request)
    {
        if($request->has('submitadd'))
        {
            $name = 'fileuploadadd';
            $this->validate($request, [
                'tenspadd' => 'required',
                'motaadd' => 'required',
                'giagocadd' => 'required',
                'giakmadd' => 'required',
                'spmoiadd' => 'required',
                'theloaiadd' => 'required',
                'fileuploadadd' => 'required|image|max:10028'
            ], 
            [
                'tenspadd.required' => 'Tên sản phẩm không được để trống',
                'motaadd.required' => 'Mô tả sản phẩm không được để trống',
                'giagocadd.required' => 'Giá gốc sản phẩm không được để trống',
                'giakmadd.required' => 'Giá khuyến mãi sản phẩm không được để trống',
                'spmoiadd.required' => 'Sản phẩm mới không được để trống',
                'fileuploadadd.required' => 'File ảnh không được để trống',
                'fileuploadadd.image' => 'Không phải định dạng ảnh',
                'fileuploadadd.max' => 'File ảnh tối đa 10M'
            ]);

            if ($request->file($name)->isValid()){
                // Lấy tên file
                $file_name = $request->file($name)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name)->move('source/image/product',$file_name);

                $product = new Product();
                $product->name = $request->tenspadd;
                $product->id_type = $request->theloaiadd;
                $product->description = $request->motaadd;
                $giagoc = str_replace(".","",$request->giagocadd);
                $product->unit_price = $giagoc;
                $giakm = str_replace(".","",$request->giakmadd);
                $product->promotion_price = $giakm;
                $product->image = $file_name;
                $product->unit = "Sản phẩm";
                $product->new = $request->spmoiadd;;
                $product->save();

                return redirect('admin/sanpham')->with('thongbao', 'Thêm sản phẩm thành công!');
            }
        }

    }

    public function postXoaSanPham(Request $request)
    {
        if($request->has('xoasp'))
        {
            $product = Product::where('id', $request->idxoa)->delete();
            return redirect('admin/sanpham')->with('thongbao', 'Xóa sản phẩm thành công!');

        }
    }

    public function getSlide()
    {
        if(Auth::check())
            redirect('admin.login');
        $slides = Slide::all();
        return view('admin/slide', compact('slides'));
    }

    public function postThemSlide(Request $request)
    {
        if($request->has('submitadd'))
        {
            $name = 'fileuploadadd';
            $this->validate($request, [
                'fileuploadadd' => 'required|image|max:10028'
            ], 
            [
                'fileuploadadd.required' => 'File ảnh không được để trống',
                'fileuploadadd.image' => 'Không phải định dạng ảnh',
                'fileuploadadd.max' => 'File ảnh tối đa 10M'
            ]);

            if ($request->file($name)->isValid()){
                // Lấy tên file
                $file_name = $request->file($name)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name)->move('source/image/slide',$file_name);

                $slide = new Slide();
                $slide->link = 'source/image/product/slide' . $file_name;
                $slide->image = $file_name;
                $slide->save();

                return redirect('admin/slide')->with('thongbao', 'Thêm ảnh trình chiếu thành công!');
            }
        }
    }

    public function postXoaSlide(Request $request)
    {
        if($request->has('xoaslide'))
        {
            $slide = Slide::where('id', $request->idxoa)->delete();
            return redirect('admin/slide')->with('thongbao', 'Xóa ảnh trình chiếu ảnh thành công!');

        }
    }

    public function postCapNhatSlide(Request $request)
    {
        if($request->has('submit'))
        {
            $name = 'fileupload';
            $this->validate($request, [
                'fileupload' => 'required|image|max:10028'
            ], 
            [
                'fileupload.required' => 'File ảnh không được để trống',
                'fileupload.image' => 'Không phải định dạng ảnh',
                'fileupload.max' => 'File ảnh tối đa 10M'
            ]);

            if ($request->file($name)->isValid()){
                // Lấy tên file
                $file_name = $request->file($name)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name)->move('source/image/slide',$file_name);

                //$slide = new Slide();
                $slide = Slide::find($request->idedit);
                $slide->link = 'source/image/product/slide' . $file_name;
                $slide->image = $file_name;
                $slide->save();

                return redirect('admin/slide')->with('thongbao', 'Cập nhật trình chiếu thành công!');
            }
            return redirect('admin/slide')->with('thongbao', 'Cập nhật trình chiếu ảnh không thành công!');

        }
    }

    public function getDonHang()
    {
        $bill_details = BillDetail::join('bills', 'bill_detail.id_bill', '=', 'bills.id')->join('customer', 'bills.id_customer', '=', 'customer.id')->where('confirm', 0)->get();
        $bill_details_proccess = BillDetail::join('bills', 'bill_detail.id_bill', '=', 'bills.id')->join('customer', 'bills.id_customer', '=', 'customer.id')->where('confirm', 1)->get();
        $bill_details_success = BillDetail::join('bills', 'bill_detail.id_bill', '=', 'bills.id')->join('customer', 'bills.id_customer', '=', 'customer.id')->where('confirm', 2)->get();
        //dd($bill_details_proccess);
        $bill = Bill::all();
        //dd($bill_details);
        
        return view('admin/donhang', compact('bill_details', 'bill_details_proccess', 'bill_details_success'));
    }

    public function postTiepNhanDonHang(Request $request)
    {
        $bill = Bill::find($request->idedit);
        $bill->confirm = 1;
        $bill->save();

        return redirect('admin/donhang')->with('thongbao', 'Tiếp nhận đơn hàng thành công!');
    }

    public function postXuLyDonHang(Request $request)
    {
        $bill = Bill::find($request->idedit1);
        $bill->confirm = 2;
        $bill->save();

        return redirect('admin/donhang')->with('thongbao', 'Đánh dấu xử lý đơn hàng thành công!');
    }

    public function getKhachHang()
    {
        $customers = Customer::all();
        return view('admin/khachhang', compact('customers'));
    }

    public function getThanhVien()
    {
        $users = User::all();
        return view('admin/thanhvien', compact('users'));
    }

    public function getLoginAdmin()
    {
        return view('admin/login');
    }

    public function postLoginAdmin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60',
            'password' => 'required|max:20'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống',
            'email.max' => 'Địa chỉ email tối đa 60 kí tự',
            'password.required' => 'Mật khẩu không được để trống',
            'password.max' => 'Mật khẩu tối đa 20 kí tự'
        ]);

        $info = array('email' => $request->email, 'password' => $request->password, 'role' => 1);
        if(Auth::attempt($info)) {
            return redirect('admin/index')->with(['flag' => 'success', 'message' => 'Đăng nhập thành công!', 'check' => "TRUE"]);
        } else {
            return redirect('admin/login')->with(['flag' => 'danger', 'message' => 'Đăng nhập không thành công vui lòng kiểm tra lại!']);
        }
    }

}
